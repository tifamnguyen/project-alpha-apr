# Generated by Django 4.2 on 2023-04-28 17:33

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0004_alter_task_due_date_alter_task_start_date"),
    ]

    operations = [
        migrations.RenameField(
            model_name="task",
            old_name="assignee",
            new_name="owner",
        ),
    ]
