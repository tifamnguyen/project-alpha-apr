from django.urls import path
from tasks.views import (
    create_my_task,
    show_my_tasks,
    edit_my_task,
)

urlpatterns = [
    path("mine/create/", create_my_task, name="create_my_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/edit", edit_my_task, name="edit_my_task"),
]
