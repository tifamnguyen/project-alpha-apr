import django_filters
from tasks.models import Task


class TaskFilterByProject(django_filters.FilterSet):
    class Meta:
        model = Task
        fields = [
            "project",
        ]
