from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, TaskEditForm
from tasks.models import Task
from tasks.filters import TaskFilterByProject
from django.contrib.auth.models import User
from projects.models import Project


@login_required
def create_my_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.owner = task.owner
            task.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm()
        form.fields["owner"].queryset = User.objects.filter(
            username=request.user
        )
    context = {
        "task_form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    task_filter = TaskFilterByProject(
        request.GET, queryset=Task.objects.filter(owner=request.user)
    )
    context = {
        "filter_form": task_filter.form,
        "tasks": task_filter.qs,
    }
    return render(request, "tasks/show_my_tasks.html", context)


@login_required
def edit_my_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskEditForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskEditForm(instance=task)
    context = {
        "task_form": form,
        "task": task,
    }
    return render(request, "tasks/edit_task.html", context)
