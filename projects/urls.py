from django.urls import path
from projects.views import (
    list_all_projects,
    list_my_projects,
    show_project,
    create_project,
    delete_project,
    edit_project_title_description,
    create_project_task,
    edit_project_task,
)

urlpatterns = [
    path("", list_all_projects, name="list_all_projects"),
    path("mine", list_my_projects, name="list_my_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path(
        "create/",
        create_project,
        name="create_project",
    ),
    path(
        "<int:id>/edit/",
        edit_project_title_description,
        name="edit_project_title_description",
    ),
    path("<int:id>/delete/", delete_project, name="delete_project"),
    path(
        "<int:id>/create_task/",
        create_project_task,
        name="create_project_task",
    ),
    path(
        "task/<int:id>/edit/",
        edit_project_task,
        name="edit_project_task",
    ),
]
