from django.http import QueryDict
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm
from tasks.forms import TaskEditForm, TaskForm
from tasks.models import Task
from projects.filters import ProjectFilterByOnwer


@login_required
def list_all_projects(request):
    project_filter = ProjectFilterByOnwer(
        request.GET, queryset=Project.objects.all()
    )
    context = {
        "filter_form": project_filter.form,
        "projects": project_filter.qs,
    }
    return render(request, "projects/list_all_projects.html", context)


@login_required
def list_my_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    if request.method == "GET":
        return render(request, "projects/show_project.html", context)
    elif request.method == "PUT":
        data = QueryDict(request.body).dict()
        form = ProjectForm(data, instance=project)
        if form.is_valid():
            form.save()
            return render(
                request,
                "projects/partials/project_title_description.html",
                context,
            )
    else:
        context = {
            "project_form": form,
        }
        return render(
            request,
            "projects/partials/edit_project_title_description.html",
            context,
        )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = project.owner
            project.save()
            return redirect("show_project", id=project.id)
    else:
        form = ProjectForm()
    context = {
        "project_form": form,
    }
    return render(request, "projects/create_project.html", context)


@login_required
def edit_project_title_description(request, id):
    project = get_object_or_404(Project, id=id)
    form = ProjectForm(instance=project)
    context = {
        "project_form": form,
        "project": project,
    }
    return render(
        request,
        "projects/partials/edit_project_title_description.html",
        context,
    )


@login_required
def delete_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        project.delete()
        return redirect("home")
    return render(request, "projects/delete_project.html")


@login_required
def create_project_task(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.owner = task.owner
            task.save()
            return redirect("show_project", id=task.project.id)
    else:
        form = TaskForm()
        form.fields["project"].queryset = Project.objects.filter(id=project.id)
    context = {
        "task_form": form,
    }
    return render(request, "projects/create_project_task.html", context)


@login_required
def edit_project_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskEditForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", id=task.project.id)
    else:
        form = TaskEditForm(instance=task)
        form.fields["project"].queryset = Project.objects.filter(
            id=task.project.id
        )
    context = {
        "task_form": form,
        "task": task,
    }
    return render(request, "projects/edit_task.html", context)
