import django_filters
from projects.models import Project


class ProjectFilterByOnwer(django_filters.FilterSet):
    class Meta:
        model = Project
        fields = [
            "owner",
        ]
